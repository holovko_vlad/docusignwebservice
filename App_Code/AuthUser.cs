﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for AuthUser
/// </summary>
public class AuthUser : System.Web.Services.Protocols.SoapHeader
{
        private string _username;
        private string _password;

        public AuthUser()
        {
            _username =  System.Web.Configuration.WebConfigurationManager.AppSettings["username"];
            _password = System.Web.Configuration.WebConfigurationManager.AppSettings["password"];
        }

        public string UserName { get; set; }
        public string Password { get; set; }

        public bool IsValid()
        {
            if (_username == "" || _password == "")
            {
                throw new Exception("Cannot get credentials!");
            }

            if (UserName == _username && Password == _password)
            {
                return true;
            }
            else
            {
                return false;
            }

        }

}