﻿using System;
using System.Text;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

using System.Data.SqlClient;

using System.Xml;
using System.Xml.Serialization;
using System.Xml.Linq;

using System.Web.Services;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

using SourceCode.SmartObjects.Client;
using SourceCode.Workflow.Client;
using SourceCode.Workflow.Management;
using SourceCode.SmartObjects.Client.Filters;
using SourceCode.Hosting.Client.BaseAPI;
using System.Runtime.Serialization.Formatters.Soap;
using Hash;


/// <summary>
/// Summary description for DocuSignProcessing
/// DocuSignProcessing - post method for push messages from DocuSignConnect
/// </summary>
[WebService(Namespace = "https://gbrx.com/webservices/docusignconnect")] // 
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class DocuSignProcessing : System.Web.Services.WebService
{

    #region Initialize variables
    public bool debugMode = false;
    public string dbConnectionString;     
    public string k2ConnectionString;  
    public string k2ConnectionStringForTask;
    public string k2UserID;
    public string k2Password;
    public string k2Host;
    public string k2WorkflowProcessName;

    public string docuSignUserName;
    public string docuSignPassword;
    public string docuSignURL;
    public string docuSignIntegratorKey;

    //public AuthUser User = new AuthUser();
    #endregion

    #region Constructor
    public DocuSignProcessing()
    {
        // get configurations
        dbConnectionString = System.Web.Configuration.WebConfigurationManager.AppSettings["dbConnectionString"];
        k2ConnectionString = System.Web.Configuration.WebConfigurationManager.AppSettings["k2ConnectionString"];
        k2ConnectionStringForTask = System.Web.Configuration.WebConfigurationManager.AppSettings["k2ConnectionStringForTask"];
        k2UserID = System.Web.Configuration.WebConfigurationManager.AppSettings["k2UserID"];
        k2Password = Hash.Hash.Decrypt(System.Web.Configuration.WebConfigurationManager.AppSettings["k2Password"]);
        k2ConnectionString = k2ConnectionString + "UserID=" + k2UserID + ";Password=" + k2Password;
        k2ConnectionStringForTask = k2ConnectionStringForTask + "UserID=" + k2UserID + ";Password=" + k2Password;
        k2Host = System.Web.Configuration.WebConfigurationManager.AppSettings["k2Host"];
        k2WorkflowProcessName = System.Web.Configuration.WebConfigurationManager.AppSettings["k2WorkflowProcessName"];
        debugMode = Convert.ToBoolean(System.Web.Configuration.WebConfigurationManager.AppSettings["debug"]);

        // for DocuSign
        docuSignUserName = System.Web.Configuration.WebConfigurationManager.AppSettings["docuSignUserName"];
        docuSignPassword = Hash.Hash.Decrypt(System.Web.Configuration.WebConfigurationManager.AppSettings["docuSignPassword"]);
        docuSignURL = System.Web.Configuration.WebConfigurationManager.AppSettings["docuSignURL"];
        docuSignIntegratorKey = System.Web.Configuration.WebConfigurationManager.AppSettings["docuSignIntegratorKey"];

    }
    #endregion

    #region WebService Methods
    [WebMethod]
    public string DocuSignConnectUpdate() //string status, string envID
    {
        string response = "";
        string status = "";
        string envID = "";

        #region get data from request
        try
        {
            System.Web.HttpRequest Request = this.Context.ApplicationInstance.Request;

            string documentContents;

            using (Stream receiveStream = Request.InputStream)
            {
                using (StreamReader readStream = new StreamReader(receiveStream, Encoding.UTF8))
                {
                    documentContents = readStream.ReadToEnd();
                }
            }
            //createLog("request: " + documentContents);
            XDocument xDoc = XDocument.Load(new StringReader(documentContents));

            var unwrappedResponse = xDoc.Descendants((XNamespace)"http://schemas.xmlsoap.org/soap/envelope/" + "Body")
                .First()
                .FirstNode;

            string DocuSignEnvelopeInformation = unwrappedResponse.ToString();

            DocuSignEnvelopeInformation = DocuSignEnvelopeInformation.Replace("xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns=\"http://www.docusign.net/API/3.0\"", "");
            DocuSignEnvelopeInformation = DocuSignEnvelopeInformation.Replace("xsi:nil=\"true\"", "");
            DocuSignEnvelopeInformation = DocuSignEnvelopeInformation.Replace("xmlns=\"http://www.docusign.net/API/3.0\"", "");
            DocuSignEnvelopeInformation = DocuSignEnvelopeInformation.Replace("xmlns=\"https://ds.smarty.pp.ua/\"", "");
            DocuSignEnvelopeInformation = DocuSignEnvelopeInformation.Replace("xmlns=\"https://bytezoom.com/webservices/docusignconnect\"", "");
            DocuSignEnvelopeInformation = DocuSignEnvelopeInformation.Replace("xmlns=\"https://gbrx.com/webservices/docusignconnect\"", "");
         

            XmlDocument body = new XmlDocument();
            body.LoadXml(DocuSignEnvelopeInformation);

            // get status
            XmlNodeList xnList = body.SelectNodes("/DocuSignConnectUpdate/DocuSignEnvelopeInformation/EnvelopeStatus/RecipientStatuses/RecipientStatus/Status");
            status = xnList[0].InnerXml;

            // get id
            XmlNodeList xnList2 = body.SelectNodes("/DocuSignConnectUpdate/DocuSignEnvelopeInformation/EnvelopeStatus/EnvelopeID");
            envID = xnList2[0].InnerXml;
        }
        catch (Exception ex)
        {
            string error = "ERROR. Get data from request has error: " + ex.ToString();
            CreateLog(error);
            return CreateSoapMessage("error");
        }
        #endregion

        //debug
        if (debugMode) CreateLog("Init Status: " + status);

        using (SqlConnection connection = new SqlConnection(dbConnectionString))
        {
            try
            {
                string powerFormId = "";
                connection.Open();

                SqlCommand command = new SqlCommand("SELECT TOP 1 ID, K2TaskStatus,PowerFormID FROM Envelopes WHERE EnvelopeID = \'" + envID + "\' ORDER BY ID DESC", connection);
                SqlDataReader dataReader = command.ExecuteReader();

                if (dataReader.HasRows && dataReader.Read())
                {
                    int ID = Convert.ToInt32(dataReader[0].ToString());
                    string K2TaskStatus = dataReader[1].ToString();
                    string powerFormId_ = dataReader[2].ToString();

                    //debug
                    if (debugMode) CreateLog("first if " + ID.ToString() + " " + K2TaskStatus + " " + powerFormId_);

                    response = UpdateDocuSignEnvelope(ID, envID, status);

                    // for envelope 
                    if (status == "Completed" && K2TaskStatus == "New" && String.IsNullOrEmpty(powerFormId_))
                    {
                        //debug
                        if (debugMode) CreateLog("powerFormId is null " + powerFormId_);
                        CompleteK2TaskByEnvelopeID(ID, envID);
                    }
                    // for powerform
                    else if (status == "Completed" && K2TaskStatus == "New" && !String.IsNullOrEmpty(powerFormId_))
                    {
                        //debug
                        if (debugMode) CreateLog("powerFormId is not null " + powerFormId_);
                        CompleteK2TaskByPowerFormID(ID, envID, powerFormId_);
                    }
                }
                else if ((powerFormId = FindEnvelopeInPowerForms(envID)) != "")
                {
                    //debug
                    if (debugMode) CreateLog("second if (found powerForm) / insert " + powerFormId);
                    response = InsertDocuSignEnvelope(envID, status, powerFormId);
                }
                else
                {
                    //debug
                    if (debugMode) CreateLog("third if(not record and not found powerForm)");
                    response = InsertDocuSignEnvelope(envID, status);
                }

            }
            catch (Exception ex)
            {
                string error = "ERROR. DocuSignConnectUpdate has error: " + ex.ToString();
                CreateLog(error);
                return CreateSoapMessage("error");
            }
            finally
            {
                if (connection != null) connection.Close();
            }
        }
        //debug
        if (debugMode) CreateLog("Response from DocuSignConnectUpdate method");
        return CreateSoapMessage(response);

    }
    #endregion

    #region K2 API Methods
    //[WebMethod]
    public string CompleteK2TaskByEnvelopeID(int ID, string envelopeID)
    {
        // debug
        if (debugMode) CreateLog("Entry to method CompleteK2TaskByEnvelopeID");
        return CompleteK2TaskGeneral(ID, envelopeID);
    }

    //[WebMethod]
    public string CompleteK2TaskByPowerFormID(int ID, string envelopeID, string powerFormID)
    {
        // debug
        if (debugMode) CreateLog("Entry to method CompleteK2TaskByPowerFormID");
        return CompleteK2TaskGeneral(ID, envelopeID, powerFormID);
    }

    public string CompleteK2TaskGeneral(int ID, string envelopeID = "", string powerFormID = "")
    {
        // debug
        if (debugMode) CreateLog("Entry to method CompleteK2TaskGeneral");

        WorkflowManagementServer workflowServer = new WorkflowManagementServer();
        string SN_ = "";
        string user = "";
        string action = "";
        string response = "";

        try
        {
            WorklistItems worklistItems = null;
            string envelopeID_ = "";
            string powerFormID_ = "";

            workflowServer.CreateConnection();
            workflowServer.Connection.Open(k2ConnectionString);

            // get variable
            StringTable st = workflowServer.GetStringTable("Development");
            user = st["DocuSignUser"].Value;

            // get processinstances
            SourceCode.Workflow.Management.Criteria.ProcessInstanceCriteriaFilter critFilter_ = new SourceCode.Workflow.Management.Criteria.ProcessInstanceCriteriaFilter();
            critFilter_.AddRegularFilter(SourceCode.Workflow.Management.ProcessInstanceFields.ProcessFullName, SourceCode.Workflow.Management.Criteria.Comparison.Equals, "AFEProject\\AFEProcess");
            ProcessInstances procInsts = workflowServer.GetProcessInstancesAll(critFilter_);
            //

            Connection k2Connection = new Connection();
            //SourceCode.Workflow.Client.ConnectionSetup con_setup= new ConnectionSetup();
            //con_setup.ConnectionString = k2ConnectionString;
            k2Connection.Open(k2Host, k2ConnectionStringForTask);
            //k2Connection.ImpersonateUser(user);

            SourceCode.Workflow.Client.ProcessInstance procInstClient;
            int processInstanceID = 0;
            string folio = "";

            // debug
            if (debugMode) CreateLog("before foreach procInsts");

            foreach (SourceCode.Workflow.Management.ProcessInstance procInst in procInsts)
            {
                // debug
                //if (debugMode) createLog("procInsts foreach: " + procInst.ID.ToString());

                procInstClient = k2Connection.OpenProcessInstance(procInst.ID);

                // dont have powerFormID
                if (powerFormID == "")
                {
                    envelopeID_ = procInstClient.DataFields["EnvelopeID"].Value.ToString();
                    if (envelopeID_ == envelopeID)
                    {
                        processInstanceID = procInst.ID;
                        folio = procInst.Folio;
                        // debug
                        if (debugMode) CreateLog("found envelope item");
                        break;
                    }
                }
                else
                {
                    powerFormID_ = procInstClient.DataFields["PowerFormID"].Value.ToString();
                    if (powerFormID_ == powerFormID)
                    {
                        processInstanceID = procInst.ID;
                        folio = procInst.Folio;
                        // debug
                        if (debugMode) CreateLog("found powerform item");
                        break;
                    }
                }

            }

            // get worklistitems
            SourceCode.Workflow.Management.Criteria.WorklistCriteriaFilter critFilter = new SourceCode.Workflow.Management.Criteria.WorklistCriteriaFilter();
            critFilter.AddRegularFilter(SourceCode.Workflow.Management.WorklistFields.ProcessFullName, SourceCode.Workflow.Management.Criteria.Comparison.Equals, k2WorkflowProcessName);

            critFilter.AddRegularFilter(SourceCode.Workflow.Management.WorklistFields.Folio, SourceCode.Workflow.Management.Criteria.Comparison.Equals, folio);

            worklistItems = workflowServer.GetWorklistItems(critFilter);

            // debug
            if (debugMode) CreateLog("After Foreach ProcessInstanceID: " + processInstanceID);
            if (debugMode) CreateLog("before foreach worklistItems");
            if (debugMode) CreateLog("Worklist Items Count - " + worklistItems.Count);

            // -------------------------------------------------------------------------------

            foreach (SourceCode.Workflow.Management.WorklistItem worklistItem in worklistItems)
            {
                SN_ = worklistItem.ProcInstID.ToString() + "_" + worklistItem.ActInstDestID.ToString();

                // debug
                //if (debugMode) createLog("worklistItems foreach: " + SN_);

                if (worklistItem.ProcInstID == processInstanceID)
                {
                    break;
                }
                else
                {
                    SN_ = "";
                }
            }
            //k2Connection.RevertUser();
            k2Connection.Close();

        }
        catch (Exception ex)
        {
            string error = "ERROR. CompleteK2TaskGeneral has error: " + ex.ToString();
            CreateLog(error);
            return error;
        }
        finally
        {
            workflowServer.Connection.Close();
        }

        // debug
        if (debugMode) CreateLog("SN: " + SN_);

        try
        {
            if (SN_ != "")
            {
                // debug
                if (debugMode) CreateLog("SN is not null");

                UpdateSN(ID, SN_);

                action = GetK2Action(envelopeID);

                CompleteK2TaskRequest(ID, user, SN_, action);

                response = "K2Task with SN: " + SN_ + " was completed";
            }
            else
            {
                response = "K2Task is not existing";
            }
        }
        catch (Exception ex)
        {
            string error = "ERROR. CompleteK2TaskGeneral has error: " + ex.ToString();
            CreateLog(error);
            return error;
        }

        // debug
        if (debugMode) CreateLog(response);
        return response;

    }

    public string GetK2Action(string envelopeID)
    {
        // debug
        if (debugMode) CreateLog("GetK2Action.");

        string action = "";
        SmartObjectClientServer soServer = EstablishK2Connection();

        try
        {
            using (soServer.Connection)
            {
                SmartObject smartObject = soServer.GetSmartObject("Envelope");

                SmartListMethod getList = smartObject.ListMethods["GetEnvelopeFormData"];
                smartObject.MethodToExecute = getList.Name;

                smartObject.Properties["EnvelopeID"].Value = envelopeID;

                Contains firstFilter = new Contains();
                firstFilter.Left = new PropertyExpression("Name", PropertyType.Text);
                firstFilter.Right = new ValueExpression("Action", PropertyType.Text);

                getList.Filter = firstFilter;

                SmartObjectList smoList = soServer.ExecuteList(smartObject);

                foreach (SmartObject smo in smoList.SmartObjectsList)
                {
                    action = smo.Properties["Value"].Value;
                    break;
                }
            }
        }
        catch (Exception ex)
        {
            string error = "ERROR. GetK2Action has error: " + ex.ToString();
            CreateLog(error);
            return error;
        }

        // debug
        if (debugMode) CreateLog("GetK2Action. Action: " + action);

        return action;
    }

    public void CompleteK2TaskRequest(int ID, string user, string SN, string action)
    {
        try
        {
            // code send request to K2 API
            using (Connection k2Connection = new Connection())
            {
                UpdateK2Status(ID, "Processing");

                // complete task
                k2Connection.Open(k2Host, k2ConnectionStringForTask);
                //k2Connection.ImpersonateUser(user);

                SourceCode.Workflow.Client.WorklistItem k2WLItem = k2Connection.OpenWorklistItem(SN);
                k2WLItem.Actions[action].Execute();

                // k2Connection.RevertUser();
                k2Connection.Close();

                // debug
                if (debugMode) CreateLog("CompleteK2TaskRequest. Action executed");

                UpdateK2Status(ID, "Completed");
            }
        }
        catch (Exception ex)
        {
            UpdateK2Status(ID, "Error Complete K2 Task");
            CreateLog("ERROR. CompleteK2TaskRequest has error: " + ex.ToString());
        }

    }

    #endregion

    #region Additional Classes

    [Serializable]
    [XmlRoot("Envelope")]
    public class Envelope
    {
        //int ID;
        public string EnvelopeID;
        public string Status;
        public Envelope()
        {

        }

        public Envelope(string EnvelopeID, string Status)
        {
            this.EnvelopeID = EnvelopeID;
            this.Status = Status;
        }

        public override string ToString()
        {
            return (String.Format("{0}, {1}", EnvelopeID, Status));
        }

    }

    [Serializable]
    [XmlRoot("EnvelopeList")]
    public class EnvelopeList
    {
        ArrayList row = new ArrayList();

        public void Add(Envelope env)
        {
            row.Add(env);
        }

    }

    [Serializable]
    public class Message
    {
        string message;
        public Message(string message)
        {
            this.message = message;
        }
    }

    public class PowerFormJson
    {
        //[JsonProperty("powerForm")]
        //public PowerFormJ PowerFormJ { get; set; }
        [JsonProperty("powerFormId")]
        public string powerFormId { get; set; }

        [JsonProperty("envelopeId")]
        public string envelopeId { get; set; }
    }

    public class PowerFormJ
    {
        [JsonProperty("powerFormId")]
        public string powerFormId { get; set; }

        [JsonProperty("envelopes")]
        public string[] envelopes { get; set; }

    }

    #endregion

    #region Additional Methods
    public string FindEnvelopeInPowerForms(string envelopeID)
    {
        // debug
        if (debugMode) CreateLog("FindEnvelopeInPowerForms");

        string powerFormId = "";

        try
        {
            /* old version without cache
            SmartObjectClientServer soServer = EstablishK2Connection();
            using (soServer.Connection)
            {
                SmartObject smartObject = soServer.GetSmartObject("PowerForm");
                smartObject.MethodToExecute = "GetJSONEnvelopes";
                soServer.ExecuteScalar(smartObject);
                jsonArray = smartObject.Properties["JSONResult"].Value;
            }
            */

            List<PowerFormJson> powerForms = JsonConvert.DeserializeObject<List<PowerFormJson>>(GetJSONPowerFormsWithEnvelope());

            foreach (var pf in powerForms)
            {
                if (pf.envelopeId == envelopeID)
                {
                    powerFormId = pf.powerFormId;
                    break;
                }
            }
        }
        catch (Exception ex)
        {
            string error = "ERROR. FindEnvelopeInPowerForms has error: " + ex.ToString();
            CreateLog(error);
            return error;
        }

        return powerFormId;
    }

    public string GetJSONPowerFormsWithEnvelope()
    {
        // debug
        if (debugMode) CreateLog("GetJSONPowerFormsWithEnvelope");

        string jsonResult = "";

        List<PowerFormJson> dsPowerForms = new List<PowerFormJson>();

        List<string> dsPowerFormIds = new List<string>();
        List<string> dbPowerFormIds = new List<string>();
        IEnumerable<string> diffPowerFormIds;

        // get List of powerFormIds from DocuSign
        try
        {
            dsPowerFormIds = GetDSPowerFormIDs();
        }
        catch (Exception ex)
        {
            string error = "ERROR. Select powerforms from DocuSign has error: " + ex.ToString();
            CreateLog(error);
            return error;
        }

        // get List of powerFormIds from DB 
        try
        {
            dbPowerFormIds = GetDBPowerFormIDs();
        }
        catch (Exception ex)
        {
            string error = "ERROR. Select powerforms from DocuSign has error: " + ex.ToString();
            CreateLog(error);
            return error;
        }

        // debug
        if (debugMode) CreateLog("DS powerforms: " + dsPowerFormIds.Count.ToString());
        if (debugMode) CreateLog("DB powerforms: " + dbPowerFormIds.Count.ToString());

        try
        {
            diffPowerFormIds = dsPowerFormIds.Except(dbPowerFormIds);

            // debug
            int count = diffPowerFormIds.Count();
            if (debugMode) CreateLog("API CALLS: " + count);

            foreach (string item in diffPowerFormIds)
            {
                string envID = "";

                SmartObjectClientServer soServer = EstablishK2Connection();
                using (soServer.Connection)
                {
                    SmartObject smartObject = soServer.GetSmartObject("PowerForm");
                    smartObject.Properties["PowerFormID"].Value = item;
                    smartObject.MethodToExecute = "GetEnvelopeID";

                    soServer.ExecuteScalar(smartObject);
                    envID = smartObject.Properties["EnvelopeID"].Value;
                }

                if (envID != "")
                {
                    PowerFormJson pFJ = new PowerFormJson();
                    pFJ.powerFormId = item;
                    pFJ.envelopeId = envID;

                    dsPowerForms.Add(pFJ);
                }

            }
            if (dsPowerForms.Count > 0)
            {
                jsonResult = JsonConvert.SerializeObject(dsPowerForms, Newtonsoft.Json.Formatting.Indented);
            }
        }
        catch (Exception ex)
        {
            string error = "ERROR. Select powerforms from DB has error: " + ex.ToString();
            CreateLog(error);
            return error;
        }

        return jsonResult;
    }

    public List<string> GetDSPowerFormIDs()
    {
        // debug
        if (debugMode) CreateLog("GetPowerFormIDs");

        List<string> powerFormIDs = new List<string>();
        string jsonArray = "";

        try
        {

            SmartObjectClientServer soServer = EstablishK2Connection();
            using (soServer.Connection)
            {
                SmartObject smartObject = soServer.GetSmartObject("PowerForm");
                smartObject.MethodToExecute = "GetJSONPowerForms";
                soServer.ExecuteScalar(smartObject);
                jsonArray = smartObject.Properties["JSONResult"].Value;
            }

            powerFormIDs = JsonConvert.DeserializeObject<List<string>>(jsonArray);

        }
        catch (Exception ex)
        {
            string error = "ERROR. GetPowerFormIDs has error: " + ex.ToString();
            CreateLog(error);
            return powerFormIDs;
        }

        return powerFormIDs;
    }

    public SmartObjectClientServer EstablishK2Connection()
    {
        SmartObjectClientServer soServer = new SmartObjectClientServer();
        try
        {
            soServer.CreateConnection();
            soServer.Connection.Open(k2ConnectionString);
        }
        catch (Exception ex)
        {
            CreateLog("ERROR. K2 cannot open connection. " + ex.ToString());
        }
        return soServer;
    }

    public string CreateSoapMessage(string message)
    {

        string soap = "<?xml version=\"1.0\" encoding=\"utf - 8\"?>< soap:Envelope xmlns:wsa = \"http://schemas.xmlsoap.org/ws/2004/08/addressing\" xmlns: wsse = \"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\" xmlns: wsu = \"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\" xmlns: soap = \"http://schemas.xmlsoap.org/soap/envelope/\"><soap:Header></soap:Header><soap:Body><DocuSignConnectUpdate xmlns =\"https://bytezoom.com/webservices/docusignconnect\">" + message + "</soap:Body></soap:Envelope>";
        Message m = new Message(message);
        MemoryStream memStream = null;
        string documentContents = "";
        memStream = new MemoryStream();
        SoapFormatter soapWrite = new SoapFormatter();
        soapWrite.Serialize(memStream, m);

        documentContents = Encoding.ASCII.GetString(memStream.GetBuffer());
        
        //Check for the null terminator character
        int index = documentContents.IndexOf("\0");
        if (index > 0)
        {
            documentContents = documentContents.Substring(0, index);
        }

        if (memStream != null) memStream.Close();
        if (debugMode) CreateLog("soap: " + soap);

        //return documentContents;
        return soap;

    }
    #endregion

    #region Database methods

    public List<string> GetDBPowerFormIDs()
    {
        List<string> DBPowerFormIDs = new List<string>();

        using (SqlConnection connection = new SqlConnection(dbConnectionString))
        {
            connection.Open();
            try
            {
                SqlCommand command = new SqlCommand("SELECT PowerFormID FROM Envelopes", connection);
                SqlDataReader dataReader = command.ExecuteReader();

                while (dataReader.Read())
                {
                    DBPowerFormIDs.Add(dataReader[0].ToString());
                }
            }
            catch (Exception ex)
            {
                string error = "ERROR. Select powerforms from DB has error: " + ex.ToString();
                CreateLog(error);
                return DBPowerFormIDs;
            }
            finally
            {
                if (connection != null) connection.Close();
            }

        }

        return DBPowerFormIDs;
    }

    public string InsertDocuSignEnvelope(string eid, string status, string powerFormId = "")
    {

        string response = "";
        using (SqlConnection connection = new SqlConnection(dbConnectionString))
        {
            connection.Open();
            try
            {
                SqlCommand insertCommand = new SqlCommand("INSERT INTO Envelopes (PowerFormID, EnvelopeID, EnvelopeStatus, K2TaskStatus) VALUES (@pfid, @eid, @status, @k2status)", connection);
                insertCommand.Parameters.Add(new SqlParameter("@pfid", powerFormId));
                insertCommand.Parameters.Add(new SqlParameter("@eid", eid));
                insertCommand.Parameters.Add(new SqlParameter("@status", status));
                insertCommand.Parameters.Add(new SqlParameter("@k2status", "New"));
                insertCommand.ExecuteNonQuery();
                response = "Envelope " + eid + " was inserted";
            }
            catch (Exception ex)
            {
                CreateLog("ERROR: " + ex.ToString());
                return "ERROR: " + ex.ToString();
            }
            finally
            {
                if (connection != null) connection.Close();
            }
        }

        // debug
        if (debugMode) CreateLog(response);
        return response;

    }

    public string UpdateDocuSignEnvelope(int ID, string eid, string status)
    {

        string response = "";
        using (SqlConnection connection = new SqlConnection(dbConnectionString))
        {
            connection.Open();
            try
            {
                SqlCommand updateCommand = new SqlCommand("UPDATE Envelopes SET EnvelopeStatus = @estatus WHERE ID = @id", connection);
                updateCommand.Parameters.Add(new SqlParameter("@id", ID));
                updateCommand.Parameters.Add(new SqlParameter("@estatus", status));
                updateCommand.ExecuteNonQuery();

                response = "Envelope " + eid + " was updated";
            }
            catch (Exception ex)
            {
                CreateLog("ERROR: " + ex.ToString());
                return "ERROR: " + ex.ToString();
            }
            finally
            {
                if (connection != null) connection.Close();
            }

        }
        // debug
        if (debugMode) CreateLog(response);
        return response;

    }

    public string UpdateK2Status(int ID, string k2status)
    {

        string response = "";
        using (SqlConnection connection = new SqlConnection(dbConnectionString))
        {
            connection.Open();
            try
            {
                SqlCommand updateCommand = new SqlCommand("UPDATE Envelopes SET K2TaskStatus = @estatus WHERE ID = @id", connection);
                updateCommand.Parameters.Add(new SqlParameter("@id", ID));
                updateCommand.Parameters.Add(new SqlParameter("@estatus", k2status));
                updateCommand.ExecuteNonQuery();

                response = "Record " + ID + " was updated";
            }
            catch (Exception ex)
            {
                CreateLog("ERROR: " + ex.ToString());
                return "ERROR: " + ex.ToString();
            }
            finally
            {
                if (connection != null) connection.Close();
            }

        }

        // debug
        if (debugMode) CreateLog(response);
        return response;

    }

    public string UpdateSN(int ID, string SN)
    {

        string response = "";
        using (SqlConnection connection = new SqlConnection(dbConnectionString))
        {
            connection.Open();
            try
            {
                SqlCommand updateCommand = new SqlCommand("UPDATE Envelopes SET SN = @sn WHERE ID = @id", connection);
                updateCommand.Parameters.Add(new SqlParameter("@id", ID));
                updateCommand.Parameters.Add(new SqlParameter("@sn", SN));
                updateCommand.ExecuteNonQuery();

                response = "Record " + ID + " was updated";
            }
            catch (Exception ex)
            {
                CreateLog("ERROR: " + ex.ToString());
                return "ERROR: " + ex.ToString();
            }
            finally
            {
                if (connection != null) connection.Close();
            }

        }

        // debug
        if (debugMode) CreateLog(response);
        return response;

    }

    public string CreateLog(string message)
    {
        using (SqlConnection connection = new SqlConnection(dbConnectionString))
        {
            connection.Open();
            try
            {
                SqlCommand insertCommand = new SqlCommand("INSERT INTO Logs (Message) VALUES (@message)", connection);
                insertCommand.Parameters.Add(new SqlParameter("@message", message));
                insertCommand.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                return "ERROR: " + ex.ToString();
            }
            finally
            {
                if (connection != null) connection.Close();
            }

        }

        return message;

    }
    #endregion DB methods
    
}

/*
// dont work
public string GetJSONEnvelopesFromDocuSign()
{
    // debug
    if (debugMode) createLog("GetJSONEnvelopes");

    string jsonResult = "";
    string baseURL = "";
    JObject envelopeIds;
    List<PowerFormJson> dsPowerForms = new List<PowerFormJson>();

    List<string> dsPowerFormIds = new List<string>();
    List<string> dbPowerFormIds = new List<string>();
    IEnumerable<string> diffPowerFormIds;

    // get List of powerFormIds from DocuSign
    try
    {
        string header = Helper.GetAuthHeader(docuSignUserName, docuSignPassword, docuSignIntegratorKey);
        baseURL = Helper.GetBaseUrl(header, docuSignURL).Result;

        // debug
        if (debugMode) createLog("baseURL: " + baseURL);

        JObject result = Helper.RunAsyncGet(
           Helper.GetAuthHeader(docuSignUserName, docuSignPassword, docuSignIntegratorKey),
           baseURL + "/powerforms/").Result;

        dsPowerFormIds = result["powerForms"].Select(t => (string)t["powerFormId"]).ToList();
    }
    catch (Exception ex)
    {
        string error = "ERROR. Select powerforms from DocuSign has error: " + ex.ToString();
        createLog(error);
        return error;
    }

    // get List of powerFormIds from DB 
    using (SqlConnection connection = new SqlConnection(dbConnectionString))
    {
        connection.Open();
        try
        {
            SqlCommand command = new SqlCommand("SELECT PowerFormID FROM Envelopes", connection);
            SqlDataReader dataReader = command.ExecuteReader();

            while (dataReader.Read())
            {
                dbPowerFormIds.Add(dataReader[0].ToString());
            }
        }
        catch (Exception ex)
        {
            string error = "ERROR. Select powerforms from DB has error: " + ex.ToString();
            createLog(error);
            return error;
        }
        finally
        {
            if (connection != null) connection.Close();
        }

    }

    // debug
    if (debugMode) createLog("DS powerforms: " + dsPowerFormIds.Count.ToString());
    if (debugMode) createLog("DB powerforms: " + dbPowerFormIds.Count.ToString());

    try
    {
        diffPowerFormIds = dsPowerFormIds.Except(dbPowerFormIds);

        // debug
        int count = diffPowerFormIds.Count();
        if (debugMode) createLog("API CALLS: " + count);

        foreach (string item in diffPowerFormIds)
        {
            envelopeIds = Helper.RunAsyncGet(
                Helper.GetAuthHeader(docuSignUserName, docuSignPassword, docuSignIntegratorKey),
                baseURL + "/powerforms/" + item + "/form_data").Result;

            if (envelopeIds["envelopes"] != null)
            {
                PowerFormJson Pfj = new PowerFormJson();
                PowerFormJ powerformj = new PowerFormJ
                {
                    powerFormId = item,
                    envelopes = envelopeIds["envelopes"].Select(t => (string)t["envelopeId"]).ToArray()
                };
                Pfj.PowerFormJ = powerformj;
                dsPowerForms.Add(Pfj);
            }

        }
        if (dsPowerForms.Count > 0)
        {
            jsonResult = JsonConvert.SerializeObject(dsPowerForms, Newtonsoft.Json.Formatting.Indented);
        }
    }
    catch (Exception ex)
    {
        string error = "ERROR. Select powerforms from DB has error: " + ex.ToString();
        createLog(error);
        return error;
    }

    return jsonResult;
}
*/
