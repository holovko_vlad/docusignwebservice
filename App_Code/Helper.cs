﻿using System;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json.Linq;

/// <summary>
/// Summary description for Helper
/// </summary>
public class Helper
{
    public Helper()
    { }

    public static string GetAuthHeader(string _UserName, string _Password, string _IntegratorKey)
    {
        return "{ \"Username\":\"" + _UserName + "\", \"Password\":\"" + _Password + "\", \"IntegratorKey\":\"" + _IntegratorKey + "\" }";
    }

    public static async Task<string> RunAsyncPost(string aut_header, string url, StringContent content)
    {
        HttpClient client = new HttpClient();
        client.DefaultRequestHeaders.Add("X-DocuSign-Authentication", aut_header);
        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        HttpResponseMessage response = await client.PostAsync(url, content);
        return response.StatusCode.ToString();
    }

    public static async Task<dynamic> RunAsyncPostDynamic(string aut_header, string url, StringContent content)
    {
        HttpClient client = new HttpClient();
        client.DefaultRequestHeaders.Add("X-DocuSign-Authentication", aut_header);
        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        HttpResponseMessage response = await client.PostAsync(url, content);
        string responseBody = await response.Content.ReadAsStringAsync();
        return JObject.Parse(responseBody);
    }

    public static async Task<dynamic> RunAsyncPUT(string aut_header, string url, StringContent content)
    {
        HttpClient client = new HttpClient();
        client.DefaultRequestHeaders.Add("X-DocuSign-Authentication", aut_header);
        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        HttpResponseMessage response = await client.PutAsync(url, content);
        string responseBody = await response.Content.ReadAsStringAsync();
        return JObject.Parse(responseBody);
    }

    public static async Task<dynamic> RunAsyncDELETE(string aut_header, string url, StringContent content)
    {
        HttpClient client = new HttpClient();
        client.DefaultRequestHeaders.Add("X-DocuSign-Authentication", aut_header);
        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        var request = new HttpRequestMessage
        {
            Method = HttpMethod.Delete,
            RequestUri = new Uri(url),
            Content = content
        };
        HttpResponseMessage response = await client.SendAsync(request);
        string responseBody = await response.Content.ReadAsStringAsync();
        return JObject.Parse(responseBody);
    }

    public static async Task<dynamic> RunAsyncGet(string aut_header, string url)
    {
        HttpClient client = new HttpClient();
        client.DefaultRequestHeaders.Add("X-DocuSign-Authentication", aut_header);
        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        HttpResponseMessage response = await client.GetAsync(url);
        string responseBody = await response.Content.ReadAsStringAsync();
        return JObject.Parse(responseBody);
    }

    public static async Task<byte[]> RunAsyncGetStream(string aut_header, string url)
    {
        HttpClient client = new HttpClient();
        client.DefaultRequestHeaders.Add("X-DocuSign-Authentication", aut_header);
        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        HttpResponseMessage response = await client.GetAsync(url);
        return await response.Content.ReadAsByteArrayAsync();
    }

    public static async Task<string> GetBaseUrl(string aut_header, string url)
    {
        HttpClient client = new HttpClient();
        client.DefaultRequestHeaders.Add("X-DocuSign-Authentication", aut_header);
        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        HttpResponseMessage response = await client.GetAsync(url + "/restapi/v2/login_information");
        string responseBody = await response.Content.ReadAsStringAsync();
        
        dynamic result = JObject.Parse(responseBody);
        return result["loginAccounts"][0]["baseUrl"].ToString();
    }

}
